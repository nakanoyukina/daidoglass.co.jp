<?php 
/*
 Union Theme - Version: 1.4
*/
?>

  <!-- / .l-main --></div>
<!-- / .l-contents --></div>

<!--     フッター   -->
<footer class="l-footer">
  <div class="page_top"><a href="#page"><img src="<?php echo HOME; ?>common/img/footer/foot_pagetop.png" srcset="<?php echo HOME; ?>common/img/footer/foot_pagetop.png 1x,<?php echo HOME; ?>common/img/footer/foot_pagetop@2x.png 2x" alt=""></a></div>
  <div class="footer_area">
    <div class="container-fluid">
      <div class="in">
        <div class="top_row">
          <div class="row">
            <div class="col-sm-3">
              <h2>大同硝子興業株式会社</h2>
            </div>
            <div class="col-sm-9 hidden-xs">
              <div class="f_navi">
                <ul>
                  <li><a href="<?php echo HOME; ?>">トップ</a></li>
                  <li><a href="<?php echo HOME; ?>about/">大同硝子興業について</a></li>
                  <li><a href="<?php echo HOME; ?>service/">サービス案内</a></li>
                  <li><a href="<?php echo HOME; ?>technology/">技術・設備紹介</a></li>
                  <li><a href="<?php echo HOME; ?>news/">お知らせ</a></li>
                  <li><a href="<?php echo HOME; ?>contact/">お問い合わせ</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="mid_row">
          <div class="address">
            <dl>
              <dt>本社</dt>
              <dd>〒530-0047 大阪府大阪市北区西天満2丁目6番8号 堂島ビルヂング3F<br>TEL <span data-action="call" data-tel="06-6363-2261">06-6363-2261</span></dd>
            </dl>
            <dl>
              <dt>京都工場</dt>
              <dd>〒613-0034 京都府久世郡久御山町佐山中道26<br>TEL <span data-action="call" data-tel="0774-44-9173">0774-44-9173</span></dd>
            </dl>
          </div>
        </div>
        <div class="bottom_row">
          <a href="<?php echo HOME; ?>privacy/">プライバシーポリシー</a>
          <p>&copy; DAIDO GLASS KOGYO CO.,LTD.</p>
        </div>
      </div>
    <!-- / .container-fluid --></div>
  <!-- /.footer_area --></div>
<!-- / .l-footer --></footer>
<!-- / #page --></div>

<?php wp_footer(); ?>
<?php if (is_page(23)||is_page(36)): ?>
<script>
$('input[name="your_zipcode[data][0]"]').jpostal({
     postcode : [
          'input[name="your_zipcode[data][0]"]',
          'input[name="your_zipcode[data][1]"]'
     ],
     address : {
          'input[name="your_address"]'  : '%3%4%5'
     }
});
</script>
<script type="text/javascript" src="//jpostal-1006.appspot.com/jquery.jpostal.js"></script>
<?php endif ?>
</body>
</html>