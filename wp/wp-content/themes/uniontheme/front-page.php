<?php 
/*
 Union Theme - Version: 1.4
*/
get_header(); ?>

<article class="m-index">

  <section class="idx_topics">
    <div class="container">
      <div class="idx_topics_row">
        <div class="row">
          <div class="col-sm-9">
            <h2><span>Topics</span></h2>
            <?php
              $args = array(
                'post_type' => 'post',
                'posts_per_page' => '1'
              );
              $news_query = new WP_Query($args);
              if ($news_query->have_posts()) :
              while ($news_query->have_posts()) : $news_query->the_post();
            ?>
            <div class="news_row"><a href="<?php the_permalink(); ?>">
              <div class="row">
                <div class="col-sm-2">
                  <time><?php the_time('Y.m.d') ?></time>
                </div>
                <div class="col-sm-10">
                  <p class="mb0"><?php the_title(); ?></p>
                </div>
              </div>
            </a></div>
            <?php endwhile; endif; ?>
          </div>
          <div class="recruit col-sm-3">
            <a href="recruit/">採用情報</a>
          </div>
        <!-- /.row --></div>
      <!-- /.idx_topics_row --></div>
    <!-- /.container --></div>
  <!-- /.idx_topics --></section>

  <section class="idx_about_sec">
    <div class="row">
      <div class="about_block block1 col-sm-6">
        <div class="img1"><img src="img/index/about_img1.jpg" srcset="img/index/about_img1.jpg 1x,img/index/about_img1@2x.jpg 2x" alt=""></div>
        <div class="text">
          <p>私たちは1945年創業の老舗プラスチック製品メーカーです。<br class="hidden-xs">長年培ったノウハウでお客様のご要望をカタチにします。</p>
          <div class="btn"><a href="about/">大同硝子興業について</a></div>
        </div>
      </div>
      <div class="about_block block2 col-sm-6">
        <div class="ttl">
          <h2>About</h2>
          <h3>創業70年、<br>大手企業様との信頼と実績。<br>モノづくりを<br>トータルプロデュース</h3>
        </div>
        <div class="img2"><img src="img/index/about_img2.jpg" srcset="img/index/about_img2.jpg 1x,img/index/about_img2@2x.jpg 2x" alt=""></div>
        <div class="text">
          <p>私たちが得意とする成形技術と<br class="hidden-xs">自社工場の成形機を中心とした使用設備の紹介です。</p>
          <div class="btn"><a href="technology/">技術・設備紹介</a></div>
        </div>
      </div>
    <!-- /.row --></div>
  <!-- /.idx_about_sec --></section>

  <section class="idx_products_sec">
    <div class="img_area">
      <div class="img" style="background: url(img/index/fac_img@2x.jpg);"></div>
      <div class="text">
        <h2>京都発 １５０以上の多品種成形</h2>
        <h3>Factory &amp;<br>SERVICE</h3>
      </div>
    </div>
    <div class="container">
      <div class="in">
        <p>当社の京都工場では、学校教材・食品関連製品の成形・検査を中心に、熟練の作業者が「たった１つの不良も世に出さない」という意識で、日々作業に取り組んでおります。</p>
        <ul class="row">
          <li class="col-xs-6 col-sm-2">
            <a href="technology#tech02" class="hover"><img src="img/index/fac_prd1.png" srcset="img/index/fac_prd1.png 1x,img/index/fac_prd1@2x.png 2x" alt="">
          </a></li>
          <li class="col-xs-6 col-sm-2">
            <a href="technology#tech01" class="hover"><img src="img/index/fac_prd2.png" srcset="img/index/fac_prd2.png 1x,img/index/fac_prd2@2x.png 2x" alt="">
          </a></li>
          <li class="col-xs-6 col-sm-2">
            <a href="technology#tech01" class="hover"><img src="img/index/fac_prd3.png" srcset="img/index/fac_prd3.png 1x,img/index/fac_prd3@2x.png 2x" alt="">
          </a></li>
          <li class="col-xs-6 col-sm-2">
            <a href="technology#tech02" class="hover"><img src="img/index/fac_prd4.png" srcset="img/index/fac_prd4.png 1x,img/index/fac_prd4@2x.png 2x" alt="">
          </a></li>
          <li class="col-xs-6 col-sm-2">
            <a href="technology#tech02" class="hover"><img src="img/index/fac_prd5.png" srcset="img/index/fac_prd5.png 1x,img/index/fac_prd5@2x.png 2x" alt="">
          </a></li>
          <li class="col-xs-6 col-sm-2">
            <a href="technology#tech01" class="hover"><img src="img/index/fac_prd6.png" srcset="img/index/fac_prd6.png 1x,img/index/fac_prd6@2x.png 2x" alt="">
          </a></li>
        </ul>
        <div class="service-btn"><a href="service/">サービス案内</a></div>
      <!-- /.in --></div>
    <!-- /.container --></div>
  <!-- /.idx_products_sec --></section>

  <section class="idx_rec_sec">
    <div class="row">
      <div class="col-sm-4 img1">
        <img src="img/index/rec_img1.jpg" srcset="img/index/rec_img1.jpg 1x,img/index/rec_img1@2x.jpg 2x" alt="">
      </div>
      <div class="col-sm-4 text">
        <div class="">
          <h2>
            <span class="en">Recruit</span>
            <span class="ja">光り輝くモノづくりを、共に</span>
          </h2>
          <ul>
            <li><a href="recruit#interview">先輩たちの声</a></li>
            <li><a href="recruit/">採用情報</a></li>
          </ul>
        </div>
      </div>
      <div class="col-sm-4 img2">
        <img src="img/index/rec_img2.jpg" srcset="img/index/rec_img2.jpg 1x,img/index/rec_img2@2x.jpg 2x" alt=""></div>
      </div>
    <!-- /.row --></div>
  <!-- /.idx_rec_sec --></section>

<!-- /.m-index --></article>

<?php get_footer(); ?>