<?php 
/*
 Union Theme - Version: 1.4
*/
get_header(); ?>

<article class="m-notfound">
<div class="container">

<div class="body tC">
<p>指定されたページまたはファイルは存在しません</p>

<ul>
<li>URL、ファイル名にタイプミスがないかご確認ください。</li>
<li>指定されたページは削除されたか、移動した可能性があります。</li>
</ul>

<p class="tC"><a href="<?php echo HOME?>" class="return_top">トップへ戻る</a></p>
<!-- / .body --></div>

</div>
<!-- / #notfound --></article>

<?php get_footer(); ?>