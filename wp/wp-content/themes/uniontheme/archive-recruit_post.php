<?php
/*
 Union Theme - Version: 1.4
*/
get_header(); ?>

<nav class="m-news_archive">
	<div class="container">

		<?php locate_template( array( 'dropdown-monthly.php' ), true, true ); //月別アーカイブドロップダウンを読み込む ?>
		
		<?php if (have_posts()) : ?>
		<div class="rec_post_list">
			<ul>
				<?php while (have_posts()) : the_post();
				
					locate_template( array( 'list-post-recruit.php' ), true, false ); //リストループ用テンプレートを読み込む
					
				endwhile;?>
			</ul>
		<!-- / .rec_post_list --></div>
		
		<?php else : echo '<p>まだ記事はありません。</p>'; endif; ?>

	</div>	
</nav>

<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>

<?php get_footer(); ?>
