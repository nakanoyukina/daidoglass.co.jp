/*
自作のスクリプトはここに
 */

/******/

// ヘッダー追尾
function fixed_header(){
  var menuHeight = $(".l-header").outerHeight();
  var w = $(window).width();
	if( w < 767){
		$(".main_visual").css({'margin-top': menuHeight + "px"});
	  $(".lower_ttl").css({'margin-top': menuHeight + "px"});
	  $(".lower_ttl2").css({'margin-top': menuHeight + "px"});
	  $(".lower_ttl3").css({'margin-top': menuHeight + "px"});
	}
  var start_pos = 0;
  $(window).on('scroll', function(){

    var current_pos = $(this).scrollTop();

    if (current_pos > start_pos) {
      if(!$('.drawer').hasClass('is-act')){
        if($(window).scrollTop() >= 200) {
          $(".l-header").css({"top": "-" + menuHeight + "px"});
        }
      }
    } else {
      $(".l-header").css({"top": 0 + "px"});
    }
    start_pos = current_pos;
  });
}

function main_slider(){
  $('.main_slider').flexslider({
    animation: "fade",
    controlNav: false,
    animationSpeed: 1000,
    slideshowSpeed: 4000,
    prevText: '',
    nextText: '',
  });
}

function drawer(){
  $('.menu_btn').on('click',function(){
    $(this).find('span').toggleClass('hidden');
    $(this).toggleClass('is-act');

    var h = $(".l-header").outerHeight();
    $('.drawer').css({'top': h }).toggleClass('is-act');
    $('.drawer').slideToggle('fast');
    if($('.drawer').hasClass('is-act')){
      $('.l-header').on('touchmove.noScroll', function(e) {
        e.preventDefault();
      });
    } else {
      // スクロール禁止 解除
      $('.l-header').off('.noScroll');
    }
    $('.drawer .navi').animate({scrollTop:1},'fast');
  });
}


function idx_about_height(){
	var w = $(window).width();
	if( w > 767){
		var h1 = $('.m-index .idx_about_sec .img1').height();
		var h2 = $('.m-index .idx_about_sec .img2').height();
		var h = h1 - h2;
		$('.m-index .idx_about_sec .block2 .ttl h3').css({'height': h});
	}
}


function loopslider(){

  $('.loopslider').each(function(){


    // $(this).on('mouseover',function(){
    //   $(this).find('#loopslider_wrap').pause();
    // }),
    // $(this).on('mouseout',function(){
    //   $(this).find('#loopslider_wrap').resume();
    // });


    var loopsliderWidth = $(this).width();
    var loopsliderHeight = $(this).height();
    $(this).children('ul').wrapAll('<div id="loopslider_wrap"></div>');

    var loopWidth = 0;
    var listObj = $('#loopslider_wrap').children('ul').children('li');
    var listNum = $('#loopslider_wrap').children('ul').children('li').length;
    // for (var i = 0; i < listNum; i++) {
    //   console.log(listObj.eq(i).width());
    //   loopWidth += listObj.eq(i).width();
    // }

    $('#loopslider_wrap').css({
        top: '0',
        left: '0',
        width: ((loopsliderWidth) * 2),
        height: (loopsliderHeight),
        // overflow: 'hidden',
        position: 'absolute'
    });

    $('#loopslider_wrap ul').css({
        width: loopsliderWidth
    });
    loopsliderPosition();

    function loopsliderPosition(){
        $('#loopslider_wrap').css({left:'0'});
        $('#loopslider_wrap').stop().animate({left:'-' + (loopsliderWidth) + 'px'},25000,'linear');
        setTimeout(function(){
            loopsliderPosition();
        },25000);
    };

    $('#loopslider_wrap ul').clone().appendTo('#loopslider_wrap');

  });
}


/*
呼び出しはここで
 */
;(function($){

  $(function() {
  	fixed_header();
  	main_slider();
  	drawer();
    loopslider();
  });

  $(window).on('load resize',function(){
  	idx_about_height();
  });

  $(window).scroll(function(){

  });

})(jQuery);