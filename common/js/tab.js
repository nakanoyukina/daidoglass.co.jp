document.addEventListener('DOMContentLoaded', () => {
  const tabTriggers = document.querySelectorAll('.js-tab-trigger');
  const tabTargets = document.querySelectorAll('.js-tab-target');
  //要素の数だけループ処理を行って値を取得
  for (let i = 0; i < tabTriggers.length; i++) {
    // タブメニュークリック時
    tabTriggers[i].addEventListener('click', (e) =>{
      let currentMenu = e.currentTarget;
      let currentContent = document.getElementById(currentMenu.dataset.id);
      //全てのタブメニューの'is-active'クラスを削除
      for (let i = 0; i < tabTriggers.length; i++){
        tabTriggers[i].classList.remove('is-active');
      }
      //クリックしたタブメニューにクラスを追加
      currentMenu.classList.add('is-active');
      for (let i = 0; i < tabTargets.length; i++){
        tabTargets[i].classList.remove('is-active');
      }
      if(currentContent !== null){
        currentContent.classList.add('is-active');
      }
    });
  }
});
