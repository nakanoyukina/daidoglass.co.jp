<!--START list-post-recruit.php-->
<li><a href="<?php the_permalink(); ?>">
	<div class="img">
	<?php
	if ( has_post_thumbnail() ) { //アイキャッチ画像の設定があればそちらを優先
		the_post_thumbnail('rec-thumb');
	} else { ?>
  <img src="<?php echo HOME; ?>img/thumbnail/thumbnail.jpg" srcset="<?php echo HOME; ?>img/thumbnail/thumbnail.jpg 1x,<?php echo HOME; ?>img/thumbnail/thumbnail@2x.jpg 2x" alt="">
<?php } ?>
  </div>
  <div class="text">
    <time><?php the_time('Y.m.d'); ?></time>
    <h3><?php the_title() ?></h3>
    <?php
      remove_filter ( 'the_content', 'wpautop' );
      the_excerpt();
    ?>
    <div class="btn">詳細を見る</div>
  </div>
</a></li>
<!--END list-post-recruit.php-->