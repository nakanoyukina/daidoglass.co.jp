<!--START list-post.php-->
<li><a href="<?php the_permalink(); ?>">
	<span class="date"><?php the_time('Y.m.d'); ?></span>
	<span class="title"><?php the_title(); ?></span>
</a></li>
<!--END list-post.php-->
