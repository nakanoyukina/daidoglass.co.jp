var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var uglify = require("gulp-uglify");
var csso = require('gulp-csso');
var browser = require("browser-sync");
var concat = require('gulp-concat');
var imagemin = require("gulp-imagemin");
var pngquant = require("imagemin-pngquant");
var mozjpeg = require('imagemin-mozjpeg');
var autoprefixer = require('gulp-autoprefixer');


//画像最適化のタスク
gulp.task('imagemin', function(){
  return gulp.src('img/**')
    .pipe(plumber())
    .pipe(imagemin([
       pngquant({
         quality: '65-80',
         speed: 1,
         floyd:0
       }),
       mozjpeg({
         quality:85,
         progressive: true
       }),
       imagemin.svgo(),
       imagemin.optipng(),
       imagemin.gifsicle()
     ]
  ))
  .pipe(gulp.dest('img_min'));
});


//オートリロードのタスク
gulp.task("server", function() {
    browser({
    	notify: false,
      server: {
        baseDir: "./"
      }
    });
});

gulp.task('sass', function () {
  return gulp.src('common/sass/*.scss')
    .pipe(plumber({
      errorHandler: function(err) {
        console.log(err.messageFormatted);
        this.emit('end');
      }
    }))
    .pipe(sass())
    .pipe(gulp.dest('common/css'))
    .pipe(autoprefixer({
        browsers: ['last 2 version', 'ie >= 10','iOS >= 8.1', 'Android >= 4.4'],
        cascade: false
    }))
    .pipe(csso())
    .pipe(gulp.dest('common/css/'))
    .pipe(browser.reload({stream:true}));
});

//Javascriptのタスク
gulp.task("js", function() {
  gulp.src(['common/js/*.js'])
    .pipe(plumber())
    .pipe(uglify())
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('common/js/min'))
    .pipe(browser.reload({stream:true}))
});

//HTMLのタスク
gulp.task('html', function() {
  return gulp.src(['**/*.html', '!wp/**'])
  .pipe(browser.reload({stream:true}))
});


gulp.task("default",['server'], function() {
    gulp.watch(["common/js/*.js"],["js"]);
    gulp.watch("common/sass/*.scss",["sass"]);
    gulp.watch("**/*.html",["html"]);
});
