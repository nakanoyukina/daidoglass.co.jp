<?php 
/*
 Union Theme - Version: 1.3
*/
get_header(); ?>


<?php while (have_posts()) : the_post(); ?>
<article class="m-news_single">
	<div class="container">
		<?php locate_template( array( 'dropdown-monthly.php' ), true, true ); //月別アーカイブドロップダウンを読み込む ?>

		<h2 class="ttl2 mb5"><?php the_title(); ?></h2>
		<span class="date"><?php the_time('Y.m.d'); ?></span>

		<div class="body">
		<?php the_content(); ?>
		</div>

		<?php if(is_singular( 'recruit_post' )){ /*カスタム投稿投稿タイプrecruit_post*/ ?>
			<div class="entry_btn"><a href="<?php echo HOME; ?>entry?post_id=<?php echo $post->ID;?>">応募する</a></div>
		<?php } ?>

	</div>
</article>
<?php endwhile;?>

<?php locate_template( array( 'pagenavi-default.php' ), true, true ); //ページナビテンプレートを読み込む ?>

<?php get_footer(); ?>