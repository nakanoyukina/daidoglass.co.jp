<?php 
/*
 Union Theme - Version: 2.0
*/
?>
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="ja">
<head>
<meta charset="UTF-8">
<title><?php if( is_front_page() && !is_paged() ) { echo do_shortcode('[uf_top_title]'); } else { wp_title('|',true,'right') ; echo do_shortcode('[uf_additional_title]'); } ?></title>
<?php if ( function_exists( 'display_meta_description' ) ) { echo display_meta_description(); } ?>
<?php if ( function_exists( 'display_ogp_tags' ) ) { echo display_ogp_tags(); } ?>
<meta name="keywords" content="<?php echo do_shortcode('[uf_meta_keywords]'); ?>">
<meta name="robots" content="index,follow">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="<?php echo add_canonical(); ?>">
<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<?php echo HOME; ?>common/img/ico/favicon.ico">
<link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo HOME; ?>common/img/ico/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo HOME; ?>common/img/ico/favicon.ico">
<!--  スマホ用基本 -->
<link rel="apple-touch-icon-precomposed" href="<?php echo HOME; ?>common/img/ico/apple-touch-icon-152x152.png">
<!--  iPad用基本 -->
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo HOME; ?>common/img/ico/apple-touch-icon-76x76.png">
<!--  スマホのRetina用 -->
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo HOME; ?>common/img/ico/apple-touch-icon-120x120.png">
<!--  iPadのRetina用 -->
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo HOME; ?>common/img/ico/apple-touch-icon-152x152.png">
<?php wp_head(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo do_shortcode('[uf_google_ua]'); ?>', 'auto');
  ga('require', 'displayfeatures');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');
</script>

</head>

<body <?php body_class(); ?>>
<div id="page">
<?php if(is_front_page() && !is_paged()) { /*トップページ*/ ?>
  <div class="c-floating_banner">
    <a href="contact/"><i><img src="common/img/ico/envelope-light.svg" alt=""></i>
      <span>お気軽にご相談・お問い合わせください</span></a>
    <p>TEL：06-6363-2261</p>
  </div>
<?php } ?>
<?php if(is_page(23)||is_page(25)||is_page(27)){ /*お問い合わせ*/ ?>
  <div class="c-floating_banner contact">
    <a href="contact/"><i><img src="common/img/ico/envelope-light.svg" alt=""></i>
      <span>お気軽にご相談・お問い合わせください</span></a>
    <p>TEL：06-6363-2261</p>
  </div>
  <?php } else { ?>
    <?php if(is_page() || is_archive() || is_single()){ /*固定ページ*/ ?>
    <div class="c-floating_banner">
      <a href="../contact/"><i><img src="../common/img/ico/envelope-light.svg" alt=""></i>
        <span>お気軽にご相談・お問い合わせください</span></a>
      <p>TEL：06-6363-2261</p>
    </div>
  <?php } ?>
<?php } ?>


<!--   ヘッダー   -->
<header class="l-header">
  <div class="container-fluid">
    <div class="row">
      <?php if(is_front_page() && !is_paged()) { /*トップページ*/ ?>
      <h1 class="logo col-xs-4 col-sm-3">
      	<a href="<?php echo HOME; ?>"><img src="<?php echo HOME; ?>common/img/header/head_logo.png" srcset="<?php echo HOME; ?>common/img/header/head_logo.png 1x,<?php echo HOME; ?>common/img/header/head_logo@2x.png 2x" alt=""></a>
      	<p class="hidden-xs">プラスチック成形をトータルプロデュース</p>
      </h1>
    	<?php } else { ?>
      <div class="logo col-xs-4 col-sm-3">
      	<a href="<?php echo HOME; ?>"><img src="<?php echo HOME; ?>common/img/header/head_logo.png" srcset="<?php echo HOME; ?>common/img/header/head_logo.png 1x,<?php echo HOME; ?>common/img/header/head_logo@2x.png 2x" alt=""></a>
      	<p class="hidden-xs">プラスチック成形をトータルプロデュース</p>
      </div>
    	<?php } ?>
      <div class="gnavi col-sm-6 hidden-xs">
        <ul>
          <li><a href="<?php echo HOME; ?>">トップ</a></li>
          <li><a href="<?php echo HOME; ?>about/">大同硝子興業について</a></li>
		  <li><a href="<?php echo HOME; ?>service/">サービス案内</a></li>
          <li><a href="<?php echo HOME; ?>technology/">技術・設備紹介</a></li>
          <li><a href="<?php echo HOME; ?>news/">お知らせ</a></li>
          <li><a href="<?php echo HOME; ?>contact/">お問い合わせ</a></li>
        </ul>
      </div>
      <div class="recruit col-sm-3 hidden-xs">
        <a href="<?php echo HOME; ?>recruit/">採用情報</a>
      </div>

      <div class="btn_box visible-xs">
        <div class="menu_btn">
          <span></span>
        </div>
      </div>

    </div>
  <!-- / .container-fluid --></div>
<!-- / .l-header --></header>

<div class="drawer">
  <ul class="navi">
    <li><a href="<?php echo HOME; ?>">トップ</a></li>
    <li><a href="<?php echo HOME; ?>about/">大同硝子興業について</a></li>
	<li><a href="<?php echo HOME; ?>service/">サービス案内</a></li>
    <li><a href="<?php echo HOME; ?>technology/">技術・設備紹介</a></li>
    <li><a href="<?php echo HOME; ?>news/">お知らせ</a></li>
    <li><a href="<?php echo HOME; ?>recruit/">採用情報</a></li>
    <li><a href="<?php echo HOME; ?>contact/">お問い合わせ</a></li>
  </ul>
<!-- /.drawer --></div>

<?php if(is_front_page() && !is_paged()) { /*トップページ*/ ?>
<div class="main_visual">
  <div class="main_slider flexslider">
    <ul class="slides">
      <li style="background: url(img/main/main_img1@2x.jpg);"></li>
      <li style="background: url(img/main/main_img2@2x.jpg);"></li>
      <li style="background: url(img/main/main_img4@2x.jpg);"></li>
    <!-- /.slides --></ul>
    <div class="text">
      <div class="container">
        <div class="en"><img src="img/main/main_txt.png" srcset="img/main/main_txt.png 1x,img/main/main_txt@2x.png 2x" alt=""></div>
        <div class="ja"><img src="img/main/main_txt2.png" srcset="img/main/main_txt2.png 1x,img/main/main_txt2@2x.png 2x" alt=""></div>
      </div>
    </div>
  <!-- /.mainslider flexslider --></div>
  <p class="scroll hidden-xs">Scroll</p>
</div>

<?php } else { ?>

	<?php if(is_404()){ /*404ページ*/ ?>
	<div class="lower_ttl3">
		<h1>
			<span class="en">404 Not Found</span>
			<span class="ja">ページが見つかりません</span>
		</h1>
	</div>
	
	<?php }elseif(is_singular( 'recruit_post' ) || is_post_type_archive( 'recruit_post' ) ){ /*カスタム投稿投稿タイプrecruit_post*/ ?>
	<div class="lower_ttl3">
		<h1>
			<span class="en">Recruit</span>
			<span class="ja">採用情報</span>
		</h1>
	</div>

	<?php }elseif(is_single() || is_archive()){ /*投稿*/ ?>
	<div class="lower_ttl3">
		<h1>
			<span class="en">Topics</span>
			<span class="ja">お知らせ</span>
		</h1>
	</div>

	<?php }elseif(is_page(3)){ /**/ ?>
	<div class="lower_ttl3">
		<h1>
			<span class="en">Privacy Policy</span>
			<span class="ja">プライバシーポリシー</span>
		</h1>
	</div>

	<?php }elseif(is_page(281)){ /**/ ?>
	<div class="lower_ttl3">
		<h1>
			<span class="en">Service</span>
			<span class="ja">サービス案内</span>
		</h1>
	</div>

	<?php }elseif(is_page(23)||is_page(25)||is_page(27)){ /*お問い合わせ*/ ?>
	<div class="lower_ttl3">
		<h1>
			<span class="en">Contact</span>
			<span class="ja">お問い合わせ</span>
		</h1>
	</div>

	<?php }elseif(is_page(36)||is_page(39)||is_page(41)){ /*エントリー*/ ?>
	<div class="lower_ttl3">
		<h1>
			<span class="en">Entry</span>
			<span class="ja">エントリー</span>
		</h1>
	</div>

	<?php }elseif(is_page(13)||is_page(15)||is_page(17)||is_page(19)){ /*インタビュー*/ ?>
	<div class="lower_ttl2">
	  <div class="row reverse">
	    <div class="col-sm-8">
	    	<?php if(is_page(13)){ ?>
	      <div class="img"><img src="../../img/main/interview_main01.jpg" srcset="../../img/main/interview_main01.jpg 1x,../../img/main/interview_main01@2x.jpg 2x" alt=""></div>
	    	<?php } elseif(is_page(15)) { ?>
	      <div class="img"><img src="../../img/main/interview_main02.jpg" srcset="../../img/main/interview_main02.jpg 1x,../../img/main/interview_main02@2x.jpg 2x" alt=""></div>
	    	<?php } elseif(is_page(17)) { ?>
	      <div class="img"><img src="../../img/main/interview_main03.jpg" srcset="../../img/main/interview_main03.jpg 1x,../../img/main/interview_main03@2x.jpg 2x" alt=""></div>
	    	<?php } elseif(is_page(19)) { ?>
	      <div class="img"><img src="../../img/main/interview_main04.jpg" srcset="../../img/main/interview_main04.jpg 1x,../../img/main/interview_main04@2x.jpg 2x" alt=""></div>
	    	<?php } ?>
	    </div>
	  </div>
	  <div class="container">
	    <div class="text">
	      <h1>
		    	<?php if(is_page(13)){ ?>
	        <span class="num">interview 01</span>
		    	<?php } elseif(is_page(15)) { ?>
	        <span class="num">interview 02</span>
		    	<?php } elseif(is_page(17)) { ?>
	        <span class="num">interview 03</span>
		    	<?php } elseif(is_page(19)) { ?>
	        <span class="num">interview 04</span>
		    	<?php } ?>
	        <span class="en">Staff Interview</span>
	        <span class="ja">スタッフインタビュー</span>
	      </h1>
	    </div>
	  </div>
	</div>

	<?php }elseif(is_page(11)){ /*固定ページ*/ ?>
	<div class="lower_ttl recruit">
	  <div class="loopslider">
	    <ul class="hidden-xs">
	      <li><img src="../img/main/recruit_main01.jpg" srcset="../img/main/recruit_main01.jpg 1x,../img/main/recruit_main01@2x.jpg 2x" alt=""></li>
	      <li><img src="../img/main/recruit_main02.jpg" srcset="../img/main/recruit_main02.jpg 1x,../img/main/recruit_main02@2x.jpg 2x" alt=""></li>
	      <li><img src="../img/main/recruit_main03.jpg" srcset="../img/main/recruit_main03.jpg 1x,../img/main/recruit_main03@2x.jpg 2x" alt=""></li>
	    </ul>
	    <div class="container">
		    <div class="text">
		      <h1>
		        <span class="en"><?php echo esc_attr( esc_html($post->post_name) ); ?></span>
		        <span class="ja"><?php wp_title('',true,'right'); ?></span>
		      </h1>
		    </div>
	    </div>
	  <!--/.loopslider--></div>
	</div>

	<?php }elseif(is_page()){ /*固定ページ*/ ?>
	<div class="lower_ttl <?php echo esc_attr( esc_html($post->post_name) ); ?>">
	  <div class="container">
	    <div class="text">
	      <h1>
	        <span class="en"><?php echo esc_attr( esc_html($post->post_name) ); ?></span>
	        <span class="ja"><?php wp_title('',true,'right'); ?></span>
	      </h1>
	    </div>
	  </div>
	</div>
	<?php } ?>
	
<?php } ?>

<!--    コンテンツ	-->

<div class="l-contents">
  
	<?php if(!is_front_page()){ //パンくず表示開始 ?>
	<div class="crumbs">
	<div class="container">		
	<ul>
	
	<li class="home"><a href="<?php echo HOME; ?>">HOME</a>&nbsp;-</li>
	
	<?php if( is_page() && $post->ancestors ) { $parents = array_reverse($post->ancestors); //階層化された固定ページ表示時に親ページをすべて表示
	foreach ($parents as $parent) { ?>
	<li><a href="<?php echo esc_url( get_permalink( $parent ) ); ?>"><?php echo esc_html(get_the_title($parent)); ?></a>&nbsp;-</li>
	<?php } unset($parent); ?>
	
	<?php }elseif(is_singular( 'recruit_post' ) ) { /*カスタム投稿投稿タイプworks｜カスタムタクソノミーworks-category*/ ?>
	<li><a href="<?php echo esc_html( get_post_type_archive_link( 'recruit_post' ) ); ?>"><?php echo esc_html( get_post_type_object( 'recruit_post' )->label ); ?></a>&nbsp;-</li>
	
	<?php } elseif (is_single() || is_date()) { $current_cat = get_the_category();  $current_cat = $current_cat[0]->term_id; /*詳細ページ｜日付アーカイブ*/?>
	<li><a href="<?php echo esc_html( get_category_link( $current_cat ) ); ?>"><?php echo esc_html(get_cat_name($current_cat)); ?></a>&nbsp;-</li>
	<?php } ?>
	
	<li><?php wp_title('',true,'right'); ?></li>
	
	</ul>
	</div>
	</div>
	<?php } //パンくず表示終わり?>
	
    <div class="l-main">

